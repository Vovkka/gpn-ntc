package focuspoint.com.gpn_ntc.model;

import java.util.List;

/**
 * Created by root on 22.01.17.
 */

public class Project {
    private List<Task> taskList;


    public Task get(int position){
        return taskList.get(position);
    }
    public void add(Task task){
        if (taskList == null) return;
        taskList.add(task);
    }

    public int size(){
        if (taskList == null) return 0;
        return taskList.size();
    }

}
