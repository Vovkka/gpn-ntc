package focuspoint.com.gpn_ntc.model;

import android.content.Context;

import focuspoint.com.gpn_ntc.R;

/**
 * Created by root on 21.01.17.
 */

public enum Stage {
    evaluation,
    identification,
    performance,
    done;

    public String getDescription(Context context){
        if (this == Stage.evaluation){
            return context.getResources().getString(R.string.evaluation);
        }
        if (this == Stage.identification){
            return context.getResources().getString(R.string.identification);
        }
        if (this == Stage.performance){
            return context.getResources().getString(R.string.performance);
        }
        if (this == Stage.done){
            return context.getResources().getString(R.string.done);
        }
        return null;
    }

    public static final String evaluationString = "evaluation";
    public static final String identificationString = "identification";
    public static final String performanceString = "performance";
    public static final String doneString = "done";
}
