package focuspoint.com.gpn_ntc.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.utils.JSONResourceReader;

/**
 * Created by root on 21.01.17.
 */

public class TaskDriver extends Driver<Float>{

    public float getWeight(){
        return weight;
    }

    private static List <TaskDriver> allDrivers = new ArrayList<>();


    public static List<TaskDriver> getAllDrivers() {
        return allDrivers;
    }


    public static List<TaskDriver> getAllFromJSON(Context context){
        String jsonString = (JSONResourceReader.JSONString(context.getResources(), R.raw.task_drivers));
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<TaskDriver>>(){}.getType();

        List <TaskDriver> list =  gson.fromJson(jsonString, collectionType);
        return list;
    }

    public static void saveAllToJSON(Context context){

    }






}
