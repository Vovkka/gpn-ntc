package focuspoint.com.gpn_ntc.model;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import focuspoint.com.gpn_ntc.GPNApp;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.utils.JSONResourceReader;

/**
 * Created by root on 21.01.17.
 */

public class Task {
    @SerializedName("id") private long id;
    @SerializedName("description") private String description;
    @SerializedName("hardnessCoefficient") private float hardnessCoefficient;
    @SerializedName("constantCoefficient") private float constantCoefficient;
    @SerializedName("taskDrivers") private List<TaskDriver> taskDrivers;
    @SerializedName("resourceDrivers") private List<ResourceDriver> resourceDrivers;
    @SerializedName("resources") private List<Resource> resources;
    @SerializedName("constantValue") private float constantValue;
    @SerializedName("stageString") private String stageString;
    @SerializedName("realCosts") private float realCosts;
    private Stage stage;
    private Resource currentResource;

    private float calculatedCosts; //Трудовые затраты T



    List <TaskListener> listeners = new ArrayList<>();

    public Task (String description){
        id = System.currentTimeMillis();
        this.description = description;
        resources = new ArrayList<>();
        taskDrivers = new ArrayList<>();
        resourceDrivers = new ArrayList<>();
        constantValue = 0;
        constantCoefficient = 1;
        hardnessCoefficient  = 1;
        currentResource = null;
        stage = Stage.evaluation;
        if (GPNApp.getResource() != null){
            resources.addAll(GPNApp.getResource());
        }
    }



    public void setStage(Stage stage) {
        this.stage = stage;
        if (stage.equals(Stage.evaluation)) {
            stageString = Stage.evaluationString;
        }
        if (stage.equals(Stage.identification)) {
            stageString = Stage.identificationString;
        }
        if (stage.equals(Stage.performance)) {
            stageString = Stage.performanceString;
        }
        calculateCosts();
    }

    public long getId() {
        return id;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public List<ResourceDriver> getResourceDrivers() {
        return resourceDrivers;
    }

    public List<TaskDriver> getTaskDrivers() {
        return taskDrivers;
    }

    public float getConstantCoefficient() {
        return constantCoefficient;
    }

    public float getHardnessCoefficient() {
        return hardnessCoefficient;
    }

    public String getDescription() {
        return description;
    }

    public float getConstantValue() {
        return constantValue;
    }

    public Resource getCurrentResource() {
        return currentResource;
    }

    public void setCurrentResource(Resource currentResource) {
        this.currentResource = currentResource;
        calculateCosts();
    }

    public void setHardnessCoefficient(float hardnessCoefficient) {
        this.hardnessCoefficient = hardnessCoefficient;
        calculateCosts();
    }

    public void setConstantCoefficient(float constantCoefficient) {
        this.constantCoefficient = constantCoefficient;
        calculateCosts();
    }

    public void setConstantValue(float constantValue) {
        this.constantValue = constantValue;
        calculateCosts();
    }

    public void addTaskDriver(TaskDriver driver){
        getTaskDrivers().add(driver);
        calculateCosts();
    }

    /**Заполняет веса и описания драйверов у ресурсов*/
    public void fillResources(){
        for (Resource resource : resources){
            for (ResourceDriver resourceDriver : resource.resourceDrivers){
                for (ResourceDriver driver : resourceDrivers){
                    if (driver.getId() == resourceDriver.getId()){
                        resourceDriver.setDescription(driver.getDescription());
                        resourceDriver.setWeight(driver.getWeight());
                    }
                }
            }
        }
    }


    public Stage getStage() {
        return stage;
    }

    //define stage
    public void defineStage(){
        if (stageString == null || stageString.isEmpty() || stageString.equals(Stage.evaluationString)){
            stage = Stage.evaluation;
            return;
        }

        if (stageString.equals(Stage.identificationString)){
            stage = Stage.identification;
        }

        if (stageString.equals(Stage.performanceString)){
            stage = Stage.performance;
        }

        if (stageString.equals(Stage.doneString)){
            stage = Stage.done;
        }

    }



    public String getCostString (Context context, Stage stage) {
        String value = String.valueOf((int) getCalculatedCosts());
        String info = "";

        if (stage.equals(Stage.evaluation)){
            info = context.getResources().getString(R.string.evaluation_costs);
        }

        if (stage.equals(Stage.identification)){
            info = context.getResources().getString(R.string.identification_costs);
        }

        if (stage.equals(Stage.performance)){
            info = context.getResources().getString(R.string.performance_costs);
        }

        return info + " " + value + " " + context.getString(R.string.hh);

    }

    //Получить трудозатраты для выбранного этапа
    public float getCalculatedCosts() {
        return calculatedCosts;
    }

    public float getRealCosts(){
        return realCosts;
    }


    //Установить фактические трудозатраты
    public void setRealCosts(float realCosts){
        this.realCosts = realCosts;
    }

    //Расчитать ожидаемые трудозатраты
    public void calculateCosts(){
        calculatedCosts = new SimpleCostsFormula(this, stage).calculateCosts();
        notifyListeners(false);
    }

    //Расчитать ожидаемые затраты для выбранного этапа
    public float calculateCostsforStage(Stage interestedStage){
        return  new SimpleCostsFormula(this, interestedStage).calculateCosts();
    }


   public interface CostsFormula{
        float calculateCosts();
   }

    class SimpleCostsFormula implements CostsFormula{
        private Task task;
        private Stage stage;

        public SimpleCostsFormula(Task task, Stage stage){
            this.task = task;
            this.stage = stage;
        }


        @Override
        public float calculateCosts() {



            if (stage == Stage.evaluation){
                float averageResources = Resource.getAveragePerformanceCoefficient(task.getResources(), new Resource.SimplePerfomanceFormula());
                if (averageResources == 0) return 0;

                float totalTaskDrivers = 0;

                for (TaskDriver driver : task.getTaskDrivers()){
                    totalTaskDrivers += driver.getDriverValue() * driver.getWeight();
                }
                totalTaskDrivers += task.constantValue;

                return totalTaskDrivers / averageResources * task.getConstantCoefficient() * task.getHardnessCoefficient();
            }


            if (stage == Stage.identification || stage == Stage.performance || stage == Stage.done){
                float perfomanceValue;
                Resource resource = getCurrentResource();
                if (resource == null){
                    perfomanceValue = 1;
                }else{
                    perfomanceValue = resource.getPerformanceCoefficient(new Resource.SimplePerfomanceFormula());
                }

                float totalTaskDrivers = 0;

                for (TaskDriver driver : task.getTaskDrivers()){
                    totalTaskDrivers += driver.getDriverValue() * driver.getWeight();
                }
                totalTaskDrivers += task.constantValue;

                return totalTaskDrivers / perfomanceValue * task.getConstantCoefficient() * task.getHardnessCoefficient();

            }

            return -1;
        }
    }


    //разобрать Список всех тасков из JSON
    public static List <Task> getFromJSON(Resources res){
        String jsonString = (JSONResourceReader.JSONString(res,R.raw.tasks));
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<Task>>(){}.getType();

        List <Task> list =  gson.fromJson(jsonString, collectionType);

        for (Task task : list){
            task.fillResources();
            task.defineStage();
            task.calculateCosts();
            task.setCurrentResource(task.getResources().get(0));
        }

        return list;
    }


    //Сохранить обьект в статическом списке
    public void saveToServer(){
        List <Task> list = GPNApp.getTasks();
        int position = 0;

        for (int i = 0; i < list.size(); i ++){
            if (list.get(i).getId() == getId()){
                position = i;
                break;
            }
        }
        list.remove(position);
        list.add(position,this);
    }

    //Получить список кандидатов участников задач
    public String[] getResourceNames(){
        List <Resource> list = getResources();

        String[] resourcesString = new String[list.size()];
        for (int i = 0; i < list.size(); i++){
            resourcesString [i] = list.get(i).getName();
        }

        return resourcesString;
    }


    //Оповещение об изменении
    public void notifyListeners(boolean changeDriverCount){

        if (listeners == null) return;

            for (TaskListener listener : listeners){
                listener.onTaskUpdate(changeDriverCount);
            }
    }

    //Добавить слушателя
    public void addListener(TaskListener listener){
        if (listeners == null) listeners = new ArrayList<>();
        if (!listeners.contains(listener)) listeners.add(listener);
    }

    //Удалить слушателя
    public void removeListener(TaskListener listener){
        if (listeners == null) listeners = new ArrayList<>();
        if (listeners.contains(listener)) listeners.remove(listener);
    }

    // Запарсить в JSON
    public String toJSON(){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        String json = gson.toJson(this);
        return json;
    }

// Разобрать  из JSON
    public static Task fromJSON(String JSONString){
        Gson gson = new Gson();
        return gson.fromJson(JSONString, Task.class);
    }

    public interface TaskListener{
        void onTaskUpdate(boolean changeDriversCount);
    }

}
