package focuspoint.com.gpn_ntc.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.utils.JSONResourceReader;

/**
 * Created by root on 21.01.17.
 */


//may be abstract
public class Resource{


    @SerializedName("name") private String name;
    @SerializedName("description") private String description;
    @SerializedName("resourceDrivers") List<ResourceDriver> resourceDrivers;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }


    private static List <Resource> allResources = new ArrayList<>();


    public static List<Resource> getAllResources() {
        return allResources;
    }


    public static List<Resource> getAllFromJSON(Context context){
        String jsonString = (JSONResourceReader.JSONString(context.getResources(), R.raw.resources));
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<Resource>>(){}.getType();

        List <Resource> list =  gson.fromJson(jsonString, collectionType);

        return list;
    }

    public static void saveAllToJSON(Context context){

    }

    public List<ResourceDriver> getResourceDrivers() {
        return resourceDrivers;
    }

    private String position;
    private float performanceCoefficient;

    public float getDriverValue(long id) {
        if (resourceDrivers == null) return 0;

        for (ResourceDriver driver : resourceDrivers){
            if (driver.id == id){
                return driver.getDriverValue();
            }
        }
        return 0;
    }

    public float getPerformanceCoefficient(PerfomanceFormula perfomanceFormula){
        return perfomanceFormula.calculatePerfomance(this);
    }

    public static float getAveragePerformanceCoefficient (List <Resource> resources, PerfomanceFormula perfomanceModel){

        if (resources == null || resources.size() == 0){
            return 1;
        }

        float perfomanceSum = 0;
        for (Resource resource : resources){
            perfomanceSum += resource.getPerformanceCoefficient(perfomanceModel);

        }

        return perfomanceSum / resources.size();
    }

    public interface PerfomanceFormula {
        float calculatePerfomance(Resource resource);
    }

    public static class SimplePerfomanceFormula implements PerfomanceFormula {

        @Override
        public float calculatePerfomance(Resource resource) {
            float driverSum = 0;
            for (ResourceDriver driver : resource.resourceDrivers){
                driverSum += driver.getWeight() * driver.getDriverValue();
            }
            return 1 - driverSum;
        }
    }
}
