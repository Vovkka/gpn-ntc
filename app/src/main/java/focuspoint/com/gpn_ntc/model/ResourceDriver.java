package focuspoint.com.gpn_ntc.model;


import java.util.List;

/**
 * Created by root on 21.01.17.
 */

public class ResourceDriver extends Driver <Float> {


    public static boolean checkWeightSum(List<ResourceDriver> list){
        float sum = 0;
        for(ResourceDriver driver : list){
            sum+=driver.getWeight();
        }
        return sum==1;
    }

}
