package focuspoint.com.gpn_ntc.model;

import com.google.gson.annotations.SerializedName;



/**
 * Created by root on 21.01.17.
 */



public abstract class Driver <T> {
    @SerializedName("weight") protected float weight;
    @SerializedName("value") protected T driverValue;
    @SerializedName("description") protected String description;
    @SerializedName("id") protected long id;

    public float getWeight() {
        return weight;
    }

    public long getId() {
        return id;
    }

    public T getDriverValue() {
        return driverValue;
    }

    public String getDescription() {
        return description;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setDriverValue(T driverValue) {
        this.driverValue = driverValue;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
