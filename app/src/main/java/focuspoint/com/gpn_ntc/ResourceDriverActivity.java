package focuspoint.com.gpn_ntc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.model.Resource;
import focuspoint.com.gpn_ntc.model.ResourceDriver;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

public class ResourceDriverActivity extends AppCompatActivity {

    private Task task;

    @BindView(R.id.resource_driver_list) RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_driver);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        task = GPNApp.getTask(intent.getExtras().getLong("id"));
        if (task == null) finish();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Драйверы ресурсов к задаче № " + task.getId());
        setSupportActionBar(toolbar);

        recyclerView.setAdapter(new ResourceDriversAdapter(task.getResourceDrivers()));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }



    class ResourceDriversAdapter extends RecyclerView.Adapter{

        private List<ResourceDriver> list;

        public ResourceDriversAdapter(List<ResourceDriver> list){
            this.list = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.resorce_driver_view, null);


            return new RecyclerView.ViewHolder(view) {
                @Override
                public String toString() {
                    return super.toString();
                }
            };
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            View rootView = holder.itemView;
            ResourceDriver driver = list.get(position);
            ((TextView)rootView.findViewById(R.id.description)).setText(driver.getDescription());
            ((TextView)rootView.findViewById(R.id.weight_view)).setText("Вес = " + driver.getWeight());
            ((TextView)rootView.findViewById(R.id.values)).setText(getResourcesValues(driver));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }


    private String getResourcesValues(ResourceDriver driver){

        StringBuilder sb = new StringBuilder();
        for (Resource resource: task.getResources()){
            sb.append("Значение (");
            sb.append(resource.getName());
            sb.append("): ");
            sb.append(resource.getDriverValue(driver.getId()));
            sb.append("\n");

        }
        return sb.toString();
    }




}
