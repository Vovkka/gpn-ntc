package focuspoint.com.gpn_ntc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.Resource;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 *
 */
public class Frg3Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    private List<Resource> resources;
    private Context context;

    public Frg3Adapter(List<Resource> resources, Context context){
        this.resources = resources;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView textView = new TextView(parent.getContext());

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        return new RecyclerView.ViewHolder(inflater.inflate(R.layout.item_card_big_adap3, null)){

        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
// ((TextView)holder.itemView).setText(taskList.get(position).getDescription());
        View rootView = holder.itemView;
        final Resource res = resources.get(position);



        ((TextView)rootView.findViewById(R.id.description)).setText(res.getDescription());
        ((TextView)rootView.findViewById(R.id.name)).setText(res.getName());

        if (position == 0) {
            ((TextView)rootView.findViewById(R.id.iteration)).setText("Задействован 3 раза");
        }else if(position == 1){
            ((TextView)rootView.findViewById(R.id.iteration)).setText("Задействован 4 раза");
        }

        ((TextView)rootView.findViewById(R.id.effective)).setText("Коэффициент эффективности " + String.valueOf(res.getPerformanceCoefficient(new
                Resource.SimplePerfomanceFormula())));



        ((TextView)rootView.findViewById(R.id.effective)).setText("Коэффициент эффективности " + String.format("%(.2f",((float) (0.8 + 0.2 *Math.random()))));



//        rootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, TaskActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putLong("id", res.getId());
//                intent.putExtras(bundle);
//                context.startActivity(intent);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return resources.size();
    }
}


