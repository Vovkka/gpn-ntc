package focuspoint.com.gpn_ntc.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import focuspoint.com.gpn_ntc.GPNApp;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.TaskActivity;
import focuspoint.com.gpn_ntc.model.Stage;
import focuspoint.com.gpn_ntc.model.Task;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 *
 */
public class SimilarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    private List<Task> taskList;
    private Context context;
    private Task similarTask;

    public SimilarAdapter(List<Task> taskList, Context context){
        this.taskList = taskList;
        this.context = context;
    }

    public void setSimilarTask(Task task){
        this.similarTask = task;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView textView = new TextView(parent.getContext());

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        return new RecyclerView.ViewHolder(inflater.inflate(R.layout.item_card_big_adap0, null)){

        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
// ((TextView)holder.itemView).setText(taskList.get(position).getDescription());
        View rootView = holder.itemView;
        final Task task = taskList.get(position);

        if (task.getStage() != Stage.done){
            rootView.setLayoutParams(new RecyclerView.LayoutParams(0,0));
        }

        try {
            if (!task.getDescription().startsWith(GPNApp.currentTask.getDescription().substring(0, 15))) {
                rootView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            }
        }catch (Exception e){}

        ((TextView)rootView.findViewById(R.id.description)).setText(task.getDescription());
        ((TextView)rootView.findViewById(R.id.description)).setTextColor(Color.WHITE);
        ((TextView)rootView.findViewById(R.id.description)).setBackgroundResource(R.color.blue);

        ((TextView)rootView.findViewById(R.id.hhp)).setText((int)(task.getCalculatedCosts()) + " ч/ч");
        ((TextView)rootView.findViewById(R.id.hhf)).setText((int)(task.getRealCosts()) + " ч/ч");


        TextView [] workViews = new TextView[] {(TextView)rootView.findViewById(R.id.work1), (TextView)rootView.findViewById(R.id.work2), (TextView)rootView.findViewById(R.id.work3) };
        TextView [] resViews = new TextView[] {(TextView)rootView.findViewById(R.id.res1), (TextView)rootView.findViewById(R.id.res2), (TextView)rootView.findViewById(R.id.res3) };


        for (int i = 0; i < workViews.length; i++){
            if (i < task.getTaskDrivers().size()){
                final SpannableStringBuilder text = new SpannableStringBuilder("• " + task.getTaskDrivers().get(i).getDescription());
                final ForegroundColorSpan style = new ForegroundColorSpan(context.getResources().getColor(R.color.blue));
                text.setSpan(style, 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                workViews[i].setText(text);

                workViews[i].setVisibility(View.VISIBLE);
            }else{
                workViews[i].setVisibility(View.GONE);
            }
        }

        for (int i = 0; i < resViews.length; i++){
            if (i < task.getResourceDrivers().size()){
                final SpannableStringBuilder text = new SpannableStringBuilder("• " + task.getResourceDrivers().get(i).getDescription());
                final ForegroundColorSpan style = new ForegroundColorSpan(context.getResources().getColor(R.color.blue));
                text.setSpan(style, 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                resViews[i].setText(text);
                resViews[i].setVisibility(View.VISIBLE);
            }else{
                resViews[i].setVisibility(View.GONE);
            }
        }





//        //Драйверы работы
//        ((TextView)rootView.findViewById(R.id.work1)).setText("- " + task.getTaskDrivers().get(0).getDescription());
//        ((TextView)rootView.findViewById(R.id.work2)).setText("- " + task.getTaskDrivers().get(1).getDescription());
//        ((TextView)rootView.findViewById(R.id.work3)).setText("- " + task.getTaskDrivers().get(2).getDescription());
//
//
//        //Драйверы ресурсоов
//        ((TextView)rootView.findViewById(R.id.res1)).setText("- " + task.getResourceDrivers().get(0).getDescription());
//        ((TextView)rootView.findViewById(R.id.res2)).setText("- " + task.getResourceDrivers().get(1).getDescription());
//        ((TextView)rootView.findViewById(R.id.res3)).setText("- " + task.getResourceDrivers().get(2).getDescription());
//

        rootView.setOnClickListener(view -> {
            Intent intent = new Intent(context, TaskActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("taskSavedState", task.toJSON());
            intent.putExtras(bundle);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemViewType(int position) {
        return taskList.get(position).getStage() == Stage.done ? 1 : 0;
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }
}


