package focuspoint.com.gpn_ntc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.TaskDriver;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 *
 */
public class Frg2Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    private List<TaskDriver> taskDrivers;
    private Context context;

    public Frg2Adapter(List<TaskDriver> taskDrivers, Context context){
        this.taskDrivers = taskDrivers;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView textView = new TextView(parent.getContext());

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        return new RecyclerView.ViewHolder(inflater.inflate(R.layout.item_card_big_adap2, null)){

        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
// ((TextView)holder.itemView).setText(taskList.get(position).getDescription());
        View rootView = holder.itemView;
        final TaskDriver task = taskDrivers.get(position);



        ((TextView)rootView.findViewById(R.id.description)).setText(task.getDescription());

        if (position == 0) {
            ((TextView)rootView.findViewById(R.id.iteration)).setText("Встретился 3 раза");
        }else if(position == 1){
            ((TextView)rootView.findViewById(R.id.iteration)).setText("Встретился 4 раза");
        }


        ((TextView)rootView.findViewById(R.id.weight)).setText("Вес : " + String.valueOf(task.getWeight()));


//        rootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, TaskActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putLong("id", task.getId());
//                intent.putExtras(bundle);
//                context.startActivity(intent);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return taskDrivers.size();
    }
}


