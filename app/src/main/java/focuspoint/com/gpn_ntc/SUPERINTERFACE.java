package focuspoint.com.gpn_ntc;

/**
 * Created by v_banko on 1/26/2017.
 */

public interface SUPERINTERFACE {
    void addTaskFromActivity();
    void defineFabVisibility(boolean visible);
}
