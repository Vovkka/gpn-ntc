package focuspoint.com.gpn_ntc.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Matrix;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

/**
 * Created by v_banko on 1/26/2017.
 */

public class TaskDriverChooserFullScreen extends LinearLayout {

    @BindView(R.id.header) TextView header;
    @BindView(R.id.task_driver_list) RecyclerView recyclerView;

    @BindString(R.string.variant_drivers) String variableDriversString;
    @BindString(R.string.current_drivers) String currentDriversString;

    private List <TaskDriver> list;
    private Task task;
    private Target target;
    private ActionListener listener;
    private TAdapter adapter;


    public TaskDriverChooserFullScreen(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.task_driver_chooser_fullscreen, this);
        ButterKnife.bind(this);


        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }


    public TaskDriverChooserFullScreen setTarget(Target target){
        this.target = target;
        return this;
    }


    public TaskDriverChooserFullScreen setDrivers(List<TaskDriver> list){
        this.list = list;
        adapter = new TAdapter(list);
        recyclerView.setAdapter(adapter);
        return this;
    }

    public TaskDriverChooserFullScreen setTask (Task task){
        this.task = task;
        return this;
    }


    public TaskDriverChooserFullScreen setActionListener (ActionListener listener){
        this.listener = listener;
        return this;
    }

    public void update(){
        adapter.notifyDataSetChanged();
    }


    public void initViews(){
        if (target == Target.ADD){
            header.setText(variableDriversString);
        }

        if (target == Target.REMOVE){
            header.setText(currentDriversString);
        }
    }



    public enum Target{
        ADD,
        REMOVE
    }

    public interface ActionListener {
        void onAction(TaskDriver driver, Target target);
    }



    private class TAdapter extends RecyclerView.Adapter {

        private  List <TaskDriver> list;


        public  TAdapter (List <TaskDriver> list){
            this.list = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            TaskDriverEditableItemView itemView = new TaskDriverEditableItemView(getContext());

            return new RecyclerView.ViewHolder(itemView) {
                public String toString() {return super.toString();}
            };
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            TaskDriverEditableItemView view = (TaskDriverEditableItemView) holder.itemView;
            view.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.setTask(task, list.get(position));
            view.setOnClickListener(v -> {
                if (listener != null)
                    listener.onAction(list.get(position), target);
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
