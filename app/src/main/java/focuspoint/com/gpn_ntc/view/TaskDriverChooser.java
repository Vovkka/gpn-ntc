package focuspoint.com.gpn_ntc.view;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.GPNApp;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

/**
 * Created by v_banko on 1/25/2017.
 */

public class TaskDriverChooser extends LinearLayout{

    @BindView(R.id.list) RecyclerView recyclerView;
    private Runnable onChooseListener;
    private Task task;

    public TaskDriverChooser(Context context, Task task, Runnable chooseListener) {
        super(context);
        inflate(context, R.layout.task_driver_chooser, this);
        ButterKnife.bind(this);

        this.task = task;
        this.onChooseListener = chooseListener;
        recyclerView.setAdapter(new TaskDriverChooseAdapter(task.getTaskDrivers()));


        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

    }


    public TaskDriverChooser(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TaskDriverChooser(Context context, AttributeSet attrs) {
        super(context, attrs);
    }




    class TaskDriverChooseAdapter extends RecyclerView.Adapter {
        private List <TaskDriver> list;

        public TaskDriverChooseAdapter (List<TaskDriver> list){
            this.list = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            TextView textView = new TextView(getContext());
            TaskDriverView view = new TaskDriverView(getContext());

            return new RecyclerView.ViewHolder(view) {
                @Override
                public String toString() {
                    return super.toString();
                }
            };
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            TaskDriverView view = (TaskDriverView) holder.itemView;
//            TextView textView = (TextView) holder.itemView;
            view.setOnlyDriver(list.get(position));

//            textView.setText(list.get(position).getDescription());

            view.setOnClickListener(v -> {

                task.addTaskDriver(list.get(position));
                if (onChooseListener != null){
                    onChooseListener.run();
                }

            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }





}
