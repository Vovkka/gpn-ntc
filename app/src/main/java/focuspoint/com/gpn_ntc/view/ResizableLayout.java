package focuspoint.com.gpn_ntc.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.design.widget.CoordinatorLayout;
import android.view.MotionEvent;
import android.widget.LinearLayout;



/**
 * Предназначен для корректной работы виртуальной клавиатуры в
 * полноэкранном режиме
 * <p>
 * Размещать в корне разметки XML
 */
public class ResizableLayout extends CoordinatorLayout {
    Rect r = new Rect();
    int maxSize;
    OnResizeListener listener;
    OnKeyboardOpen keyboardOpen;
    OnKeyboardClose keyboardClose;
    Runnable touchListener;
    private boolean keyboardOpened = false;

    public interface OnResizeListener {
        void onResize();
    }

    public interface OnKeyboardOpen{
        void onKeyboardOpen();
    }

    public interface OnKeyboardClose{
        void onKeyboardClose();
    }


    public ResizableLayout(Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        getWindowVisibleDisplayFrame(r);
        if (r.height() > maxSize) {
            maxSize = r.height();
        }

        if (r.height() < maxSize) {
            if (listener != null) {
                listener.onResize();
            }
            if (keyboardOpen != null && !keyboardOpened){
                keyboardOpen.onKeyboardOpen();
                keyboardOpened = true;
            }
        }else{
            if (keyboardClose!=null && keyboardOpened){
                keyboardClose.onKeyboardClose();
                keyboardOpened = false;
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setTouchListener(Runnable listener){
        this.touchListener = listener;
    }

    public void setOnResizeListener(OnResizeListener listener) {
        this.listener = listener;
    }

    public void setKeyboardOpen(OnKeyboardOpen listener){
        this.keyboardOpen = listener;
    }

    public void setKeyboardClose (OnKeyboardClose listener){
        this.keyboardClose = listener;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (touchListener != null){
            touchListener.run();
        }
        return super.onInterceptTouchEvent(ev);
    }
}