package focuspoint.com.gpn_ntc.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

/**
 * Created by v_banko on 1/24/2017.
 */

public class TaskDriverView extends LinearLayout {
    @BindView(R.id.description) TextView descriptionView;
    @BindView(R.id.driver_weight) EditText weightView;
    @BindView(R.id.driver_value) EditText valueView;

    private TaskDriver driver;
    private Task task;

    public TaskDriverView(Context context) {
        this(context, null);
        inflate(context, R.layout.task_driver_item_view, this);
        ButterKnife.bind(this);
    }

    public TaskDriverView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TaskDriverView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setTask(Task task, TaskDriver driver){
        this.driver = driver;
        this.task = task;
        descriptionView.setText(driver.getDescription());
        valueView.setText(String.valueOf(driver.getDriverValue()));
        valueView.setOnFocusChangeListener(textViewFocusListener);
//        valueView.setOnEditorActionListener(onEditorActionListener);

        weightView.setText(String.valueOf(driver.getWeight()));
        weightView.setOnFocusChangeListener(textViewFocusListener);
//        weightView.setOnEditorActionListener(onEditorActionListener);
    }

//    private TextView.OnEditorActionListener onEditorActionListener =  new TextView.OnEditorActionListener() {
//        @Override
//        public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                textView.clearFocus();
//                getRootView().requestFocus();
//
//                return false;
//            }
//            return false;
//        }
//    };


    public void setOnlyDriver(TaskDriver driver){
        this.driver = driver;
        descriptionView.setText(driver.getDescription());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        setLayoutParams(params);
        descriptionView.setLayoutParams(params);
        descriptionView.setPadding(10, 10, 10, 10);

//        valueView.setText(String.valueOf(driver.getDriverValue()));
        valueView.setVisibility(GONE);

//        weightView.setText(String.valueOf(driver.getWeight()));
        weightView.setVisibility(GONE);

    }



    private TextView.OnEditorActionListener editorDriverListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (textView.getText().length()==0){
                textView.setText("0");
                if (textView == weightView){
                    driver.setWeight(0);
                    task.calculateCosts();
                }
                if (textView == valueView){
                    driver.setDriverValue(0f);
                    task.calculateCosts();
                }
                return false;
            }

            if (i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_ACTION_NEXT){
                float value = Float.valueOf(textView.getText().toString());
                if (textView == weightView){
                    driver.setWeight(value);
                    task.calculateCosts();
                }
                if (textView == valueView){
                    driver.setDriverValue(value);
                    task.calculateCosts();
                }
            }
            return false;
        }
    };

    private View.OnFocusChangeListener textViewFocusListener = (view, b) -> {
        if (!(view instanceof EditText)) return;
        TextView textView = (TextView) view;
        if (!b) {
            if (textView.getText().length() == 0) {
                textView.setText("0");
                if (textView == weightView) {
                    driver.setWeight(0);
                    task.calculateCosts();
                }
                if (textView == valueView) {
                    driver.setDriverValue(0f);
                    task.calculateCosts();
                }
            } else {
                float value = Float.valueOf(textView.getText().toString());
                if (textView == weightView) {
                    driver.setWeight(value);
                    task.calculateCosts();
                }
                if (textView == valueView) {
                    driver.setDriverValue(value);
                    task.calculateCosts();
                }
            }
        }
    };



}
