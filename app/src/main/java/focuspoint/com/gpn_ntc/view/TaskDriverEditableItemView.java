package focuspoint.com.gpn_ntc.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

/**
 * Created by v_banko on 1/26/2017.
 */



public class TaskDriverEditableItemView extends LinearLayout {
    @BindView(R.id.description) TextView descriptionView;
    @BindView(R.id.driver_weight) TextView weightView;
//    @BindView(R.id.driver_value) EditText valueView;

    private TaskDriver driver;
    private Task task;

    public TaskDriverEditableItemView(Context context) {
        this(context, null);
        inflate(context, R.layout.task_driver_tem_view_editable, this);
        ButterKnife.bind(this);
    }

    public TaskDriverEditableItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TaskDriverEditableItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void update(){
    }

    public void setTask(Task task, TaskDriver driver){
        this.driver = driver;
        this.task = task;
        descriptionView.setText(driver.getDescription());
//        valueView.setText(String.valueOf(driver.getDriverValue()));
//        valueView.setOnFocusChangeListener(textViewFocusListener);

        weightView.setText(String.valueOf(driver.getWeight()));

    }


    public void setOnlyDriver(TaskDriver driver){
        this.driver = driver;
        descriptionView.setText(driver.getDescription());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        setLayoutParams(params);
        descriptionView.setLayoutParams(params);
        descriptionView.setPadding(10, 10, 10, 10);

//        valueView.setText(String.valueOf(driver.getDriverValue()));
//        valueView.setVisibility(GONE);

//        weightView.setText(String.valueOf(driver.getWeight()));
        weightView.setVisibility(GONE);

    }
}
