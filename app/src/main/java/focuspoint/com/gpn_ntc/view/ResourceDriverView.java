package focuspoint.com.gpn_ntc.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.model.Resource;
import focuspoint.com.gpn_ntc.model.ResourceDriver;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

/**
 * Created by root on 25.01.17.
 */

public class ResourceDriverView extends LinearLayout {

    @BindView(R.id.description) TextView descriptionView;
    @BindView(R.id.driver_weight) EditText weightView;
    @BindView(R.id.driver_value) EditText valueView;

    private ResourceDriver driver;
    private Task task;

    public ResourceDriverView(Context context) {
        this(context, null);
        inflate(context, R.layout.task_driver_item_view, this);
        ButterKnife.bind(this);
    }

    public ResourceDriverView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ResourceDriverView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setTask(Task task, ResourceDriver driver){
        this.driver = driver;
        this.task = task;
        update();

        valueView.setOnFocusChangeListener(textViewFocusListener);
        weightView.setOnFocusChangeListener(textViewFocusListener);
    }



    private View.OnFocusChangeListener textViewFocusListener = (view, b) -> {
        if (!(view instanceof EditText)) return;
        TextView textView = (TextView) view;
        if (!b) {
            if (textView.getText().length() == 0) {
                textView.setText("0");
                if (textView == weightView) {
                    driver.setWeight(0);
                    task.calculateCosts();
                }
                if (textView == valueView) {
                    driver.setDriverValue(0f);
                    task.calculateCosts();
                }
            } else {
                float value = Float.valueOf(textView.getText().toString());
                if (textView == weightView) {
                    driver.setWeight(value);
                    task.calculateCosts();
                }
                if (textView == valueView) {
                    driver.setDriverValue(value);
                    task.calculateCosts();
                }
            }
        }
    };

    private void update(){
        descriptionView.setText(driver.getDescription());

        valueView.setText(String.valueOf(driver.getDriverValue()));
        weightView.setText(String.valueOf(driver.getWeight()));
    }



}
