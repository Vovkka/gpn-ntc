package focuspoint.com.gpn_ntc.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.R;

/**
 * Created by v_banko on 1/25/2017.
 */

public class DoneTaskView extends LinearLayout{

@BindView(R.id.real_cost_view)
    EditText realCostView;
    public DoneTaskView(Context context) {
        super(context);
        inflate(context, R.layout.done_task_view, this);
        ButterKnife.bind(this);
    }

    public DoneTaskView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DoneTaskView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public float getRealCosts(){
        String costs = realCostView.getText().toString();
        if (costs.length() == 0) return 0;

        return Float.valueOf(costs);

    }


}
