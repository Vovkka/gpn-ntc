package focuspoint.com.gpn_ntc;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;
import focuspoint.com.gpn_ntc.view.TaskDriverChooserFullScreen;

public class DriverChooseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.current_drivers) TaskDriverChooserFullScreen currentDriversView;
    @BindView(R.id.drivers_to_add) TaskDriverChooserFullScreen driversToAddView;
    @BindView(R.id.fab) FloatingActionButton fab;

    @BindString(R.string.no_more_drivers) String noMoredrivers;


    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_choose);
        ButterKnife.bind(this);


        String json = getIntent().getExtras().getString("taskSavedState");
        task = Task.fromJSON(getIntent().getExtras().getString("taskSavedState"));


        toolbar.setTitle("Драйверы работы");


        currentDriversView.setTarget(TaskDriverChooserFullScreen.Target.REMOVE)
                .setTask(task)
                .setDrivers(task.getTaskDrivers())
                .setActionListener(listener)
                .initViews();


        driversToAddView.setTarget(TaskDriverChooserFullScreen.Target.ADD)
                .setTask(task)
                .setDrivers(GPNApp.getDrivers())
                .setActionListener(listener)
                .initViews();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        fab.setOnClickListener(v -> saveChanges());

    }

    private void saveChanges(){

        if (task.getTaskDrivers().size() > 5 ){
            Toast.makeText(this, noMoredrivers, Toast.LENGTH_SHORT).show();
        }else{
            String json = task.toJSON();
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("taskSavedState", json);
            intent.putExtras(bundle);

            setResult(RESULT_OK, intent);
            finish();
        }


    }

    private TaskDriverChooserFullScreen.ActionListener listener = (driver, target) -> {

        if (target == TaskDriverChooserFullScreen.Target.ADD){
            task.getTaskDrivers().add(driver);
        }
        if (target == TaskDriverChooserFullScreen.Target.REMOVE){
            task.getTaskDrivers().remove(driver);
        }
        currentDriversView.update();
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(menuItem);
    }

}
