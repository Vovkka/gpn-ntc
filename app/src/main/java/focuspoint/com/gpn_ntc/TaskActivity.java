package focuspoint.com.gpn_ntc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.fragments.DataFragment;
import focuspoint.com.gpn_ntc.fragments.SimilarFragment;
import focuspoint.com.gpn_ntc.model.ResourceDriver;
import focuspoint.com.gpn_ntc.model.Stage;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.view.DoneTaskView;

public class TaskActivity extends AppCompatActivity implements SUPERINTERFACE{
    private Task task;


    @BindView(R.id.task_calculated_cost) TextView taskCalculatedCost;
    @BindView(R.id.task_real_cost) TextView taskRealCost;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.menu) FloatingActionMenu fab;

    @BindView(R.id.help)   FloatingActionButton help;
    @BindView(R.id.save)   FloatingActionButton save;
    @BindView(R.id.reload) FloatingActionButton reload;
    @BindView(R.id.check)  FloatingActionButton check;

    @BindString(R.string.hh) String hh;





    private static AlertDialog chooseDialog;
    private String taskSavedState;
    private boolean changes = false;
    private long start;

    private SimilarFragment similarFragment = new SimilarFragment();
    private DataFragment dataDriverFragment = new DataFragment();
    private DataFragment dataResourceFragment = new DataFragment();
    private DataFragment dataCoefficientFragment = new DataFragment();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        ButterKnife.bind(this);
        start = System.currentTimeMillis();


        taskSavedState = getIntent().getExtras().getString("taskSavedState");
        task = Task.fromJSON(taskSavedState);

        GPNApp.currentTask = task;

        if (task == null) finish();




        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setCostsInfo();



        ////////////////////////////////////////////



//        rootView.setKeyboardClose(this::takeFocusBack);
//        rootView.setKeyboardOpen(() -> {});
//
//        hadnessCoeffEditView.setOnFocusChangeListener(textViewFocusListener);
//        constantCoeffEditView.setOnFocusChangeListener(textViewFocusListener);
//        constantValueEditView.setOnFocusChangeListener(textViewFocusListener);
//
//        resourceFrame.setOnClickListener(view -> {});

//        String json = task.toJSON();




        help.setOnClickListener(v -> help());
        save.setOnClickListener(v -> save());
        reload.setOnClickListener(v -> reload());
        check.setOnClickListener(v -> check());


        fab.setOnClickListener(view -> onDone());
//        fabTaskDriver.setOnClickListener(view -> addTaskFromActivity());
//        fabTaskDriver.setOnClickListener(view -> addTaskDriver());

//        fillData(true);
//        resourceChooser();
        toolbar.setTitle(task.getDescription());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        new Handler().postDelayed(() -> changes = false, 1000);
//        start = System.currentTimeMillis();
    }

    private void help(){
        Intent intent = new Intent(this, FormulaActivity.class);
        startActivity(intent);

    }
    private void save(){
        changes = false;
        saveState();
    }
    private void reload(){
        dataDriverFragment.fillData(true);
        dataResourceFragment.fillData(true);
        dataCoefficientFragment.fillData(true);
    }
    private void check(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DoneTaskView view = new DoneTaskView(this);
        builder.setView(view);
        builder.setPositiveButton("Завершить", (dialogInterface, i) -> {
            task.setRealCosts(view.getRealCosts());
            task.setStage(Stage.done);
            dialogInterface.dismiss();
            save();
            finish();
        });
        builder.setNegativeButton("Отмена", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        builder.create().show();
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null){
            changes = true;
            taskSavedState = data.getStringExtra("taskSavedState");
            task = Task.fromJSON(taskSavedState);

            dataDriverFragment.setTask(task);
            dataResourceFragment.setTask(task);
            dataCoefficientFragment.setTask(task);



            dataDriverFragment.fillData(true);
            dataResourceFragment.fillData(true);
            dataCoefficientFragment.fillData(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        if (menuItem.getItemId() == android.R.id.home) {
            if (changes && System.currentTimeMillis() - start > 1000) {

                showOnExitDialog();
            } else {
                finish();
            }

        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onStart() {
        task.addListener(taskListener);
        super.onStart();
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        dataDriverFragment.setTask(task);
        dataResourceFragment.setTask(task);
        dataCoefficientFragment.setTask(task);
        dataDriverFragment.setSuperInterface(this);
        dataResourceFragment.setSuperInterface(this);
        dataCoefficientFragment.setSuperInterface(this);

        dataDriverFragment.setTabType(DataFragment.DRIVER_TAB);
        dataResourceFragment.setTabType(DataFragment.RESOURCE_TAB);
        dataCoefficientFragment.setTabType(DataFragment.COEFFICIENT_TAB);



        adapter.addFragment(dataDriverFragment, "ДРАЙВЕРЫ");
        adapter.addFragment(dataResourceFragment, "РЕСУРСЫ");
        adapter.addFragment(similarFragment, "АНАЛОГИ");
        adapter.addFragment(dataCoefficientFragment, "КОЭФФИЦИЕНТЫ");

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

//    private String getTaskDriversString(){
//        StringBuilder builder = new StringBuilder();
//        for (TaskDriver driver : task.getTaskDrivers()){
//            builder.append("- ");
//            builder.append(driver.getDescription());
//            builder.append("\n");
//        }
//
//        return builder.toString();
//    }

//    private void fillTaskDriversList() {
//        taskDriversView.removeAllViews();
//
//        new Handler().post(() -> {
//            for (TaskDriver driver : task.getTaskDrivers()) {
//                TaskDriverView view = new TaskDriverView(TaskActivity.this);
//                view.setTask(task, driver);
//                taskDriversView.addView(view);
//            }
//            taskDriversView.requestLayout();
//        });
//
//    }
//
//    private void fillResourceDriversList() {
//        resourceDriversView.removeAllViews();
//        if (task.getCurrentResource() == null) return;
//        new Handler().post(() -> {
//            for (ResourceDriver driver : task.getCurrentResource().getResourceDrivers()) {
//                ResourceDriverView view = new ResourceDriverView(TaskActivity.this);
//                view.setTask(task, driver);
//                resourceDriversView.addView(view);
//            }
//            resourceDriversView.requestLayout();
//        });
//    }


    private String getResourceDriversString() {
        StringBuilder builder = new StringBuilder();
        for (ResourceDriver driver : task.getResourceDrivers()) {
            builder.append("- ");
            builder.append(driver.getDescription());
            builder.append("\n");
        }

        return builder.toString();
    }

    private String getResourceNameString() {
        return task.getResources().get(0).getName();
    }

    private void goToResourceDrivers() {
        Intent intent = new Intent(this, ResourceDriverActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

    private void goToTaskDrivers() {
        Intent intent = new Intent(this, TaskDriverActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

//    private void takeFocusBack() {
//        new Handler().postDelayed(() -> rootView.clearFocus(), 200);
//    }

//    private Task.TaskListener taskListener = changeDriversCount -> {
//        System.out.println("CHANGES");
//        changes = true;
//        fillData(changeDriversCount);
//    };

//    private void fillData(boolean updateLists) {
//        System.out.println("FILL DATA");
//
//        if (task.getCurrentResource() != null && !ResourceDriver.checkWeightSum(task.getCurrentResource().getResourceDrivers())) {
//            Toast.makeText(this, "Сумма весов должна быть равна 1", Toast.LENGTH_LONG).show();
//        }
//
//        toolbar.setTitle(task.getDescription());
////        toolbar.setTitle("Этап: " + task.getStage().getDescription(this));
//        resourceView.setClickable(task.getStage() != Stage.done);
//        resourceView.setEnabled(task.getStage() != Stage.done);
//
//        fabTaskDriver.setVisibility(task.getStage() != Stage.done ? View.VISIBLE : View.GONE);
//
//        description.setText(task.getDescription());
//        description.setVisibility(View.GONE);
//
//        hadnessCoeffView.setText(hardCoeffString);
//        constantCoeffView.setText(constCoeffString);
//        constantValueView.setText(constValueString);
//
//
//        if (updateLists) {
//            fillTaskDriversList();
//            fillResourceDriversList();
//        }
//
//
//        tasksStageView.setVisibility(View.GONE);
//        taskCalculatedCost.setText("Плановые трудозатраты: " + (int) task.getCalculatedCosts() + " " + hh);
//        taskRealCost.setText("Фактические трудозатраты: " + (int) task.getRealCosts() + " " + hh);
//
//        hadnessCoeffEditView.setText(String.valueOf(task.getHardnessCoefficient()));
//        constantCoeffEditView.setText(String.valueOf(task.getConstantCoefficient()));
//        constantValueEditView.setText(String.valueOf(task.getConstantValue()));
//        setSupportActionBar(toolbar);
//        if (task.getStage().equals(Stage.done)) {
//            fab.setVisibility(View.GONE);
//
//        }
//
//
//    }

//    private View.OnFocusChangeListener textViewFocusListener = (view, b) -> {
//        if (!(view instanceof EditText)) return;
//        TextView textView = (TextView) view;
//        if (!b) {
//            if (textView.getText().length() == 0) {
//                textView.setText("0");
//                if (textView == hadnessCoeffEditView) task.setHardnessCoefficient(0);
//                if (textView == constantCoeffEditView) task.setConstantCoefficient(0);
//                if (textView == constantValueEditView) task.setConstantValue(0);
//            } else {
//                float value = Float.valueOf(textView.getText().toString());
//                if (textView == hadnessCoeffEditView) task.setHardnessCoefficient(value);
//                if (textView == constantCoeffEditView) task.setConstantCoefficient(value);
//                if (textView == constantValueEditView) task.setConstantValue(value);
//            }
//        }
//    };

//    private void addTaskFromActivity(){
//        Intent intent = new Intent(this, DriverChooseActivity.class);
//        Bundle bundle = new Bundle();
//
//        String str = taskSavedState;
//        System.out.println(str);
//
//
//        bundle.putString("taskSavedState", taskSavedState);
//        intent.putExtras(bundle);
//
//        startActivityForResult(intent, 777);
//    }




    private void onDone() {


//        Intent intent = new Intent(this, FormulaActivity.class){
//
//        };
//


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DoneTaskView view = new DoneTaskView(this);
        builder.setView(view);
        builder.setPositiveButton("Завершить", (dialogInterface, i) -> {
            task.setRealCosts(view.getRealCosts());
            task.setStage(Stage.done);
            dialogInterface.dismiss();
        });
        builder.setNegativeButton("Отмена", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        builder.create().show();
    }

//    private void resourceChooser() {
//
//        String[] data = task.getResourceNames();
//        if (data.length == 0) return;
//
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data) {
//            @NonNull
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//
//                View view = super.getView(position, convertView, parent);
//                TextView text = (TextView) view.findViewById(android.R.id.text1);
//                text.setTextColor(Color.BLACK);
//                view.setVisibility(task.getCurrentResource() == null ? View.GONE : View.VISIBLE);
//                return view;
//            }
//        };
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        resourceView.setAdapter(adapter);
//
//        for (int i = 0; i < task.getResources().size(); i++) {
//            if (task.getResources().get(i) == task.getCurrentResource()) {
//                resourceView.setSelection(i);
//                break;
//            }
//        }
//
//        resourceView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                List<Resource> list = task.getResources();
//                if (task.getCurrentResource() != list.get(i)) {
//                    task.setCurrentResource(list.get(i));
//
//                }
//            }
//
//            public void onNothingSelected(AdapterView<?> adapterView) {
//            }
//        });
//    }


    // устанавливаем обработчик нажатия
//        resourceView.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                // показываем позиция нажатого элемента
//                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//            }
//        });


    @Override
    protected void onStop() {
        super.onStop();
        task.removeListener(taskListener);
    }

    @Override
    public void onBackPressed() {
        dataDriverFragment.takeFocusBack();
        dataCoefficientFragment.takeFocusBack();
        dataResourceFragment.takeFocusBack();
        if (changes && System.currentTimeMillis() - start > 1000) {
            showOnExitDialog();
        } else {
            super.onBackPressed();
        }

    }


    private void saveState() {
        task.saveToServer();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private void showOnExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setView(R.layout.save_on_exit_dialog);
        builder.setPositiveButton("Да", (dialogInterface, i) -> {
            saveState();
            finish();
        });
        builder.setNegativeButton("Нет", (dialogInterface, i) -> finish());

        builder.create().show();
    }


    private Task.TaskListener taskListener = changeDriversCount -> {

        if (System.currentTimeMillis() - start > 1000) {
            changes = true;
        }
        setCostsInfo();
        try{dataCoefficientFragment.fillData(changeDriversCount);}catch (Exception e){}
        try{dataResourceFragment.fillData(changeDriversCount);}catch (Exception e){}
        try{dataDriverFragment.fillData(changeDriversCount);}catch (Exception e){}
    };

    @Override
    public void addTaskFromActivity() {
        Intent intent = new Intent(this, DriverChooseActivity.class);
        Bundle bundle = new Bundle();

        String str = taskSavedState;
        System.out.println(str);


        bundle.putString("taskSavedState", taskSavedState);
        intent.putExtras(bundle);

        startActivityForResult(intent, 777);
    }

    @Override
    public void defineFabVisibility(boolean visible) {
//        fab.setVisibility(visible ? View.VISIBLE : View.GONE);
        check.setVisibility(visible ? View.VISIBLE : View.GONE);
        save.setVisibility(visible ? View.VISIBLE : View.GONE);


    }

    private void setCostsInfo(){
        taskCalculatedCost.setText("Прогнозируемые трудозатраты: " + (int) task.getCalculatedCosts() + " " + hh);
        if (task.getStage() != Stage.done){
            taskRealCost.setVisibility(View.GONE);
        }
        taskRealCost.setText("Фактические трудозатраты: " + (int) task.getRealCosts() + " " + hh);
    }


}
