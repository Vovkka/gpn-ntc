package focuspoint.com.gpn_ntc.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import focuspoint.com.gpn_ntc.GPNApp;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.adapters.Frg1Adapter;


public class Fragment1 extends Fragment {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    private Unbinder unbinder;
    private RecyclerView.Adapter mAdapter;

    public static Fragment1 newInstance() {
        return new Fragment1();
    }

    public void update(){
        if (mAdapter!= null){
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_1,  container, false);
        unbinder = ButterKnife.bind(this, view);




        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);


        //penser à passer notre Adapter (ici : Frg0Adapter) à un RecyclerViewMaterialAdapter
        mAdapter = new RecyclerViewMaterialAdapter(new Frg1Adapter(GPNApp.getTasks(), getActivity()));
        recyclerView.getWidth();
        recyclerView.setAdapter(mAdapter);

        //notifier le MaterialViewPager qu'on va utiliser une RecyclerView
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), recyclerView);





        return view;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
