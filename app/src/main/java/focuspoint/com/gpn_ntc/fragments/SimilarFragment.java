package focuspoint.com.gpn_ntc.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;

import focuspoint.com.gpn_ntc.AddTaskActivity;
import focuspoint.com.gpn_ntc.GPNApp;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.adapters.Frg0Adapter;
import focuspoint.com.gpn_ntc.adapters.SimilarAdapter;
import focuspoint.com.gpn_ntc.model.Task;

import static focuspoint.com.gpn_ntc.R.id.fab;
import static focuspoint.com.gpn_ntc.R.id.recyclerView;

/**
 *
 */
public class SimilarFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private FloatingActionButton fab;
    private Task currentTask;

    public static SimilarFragment newInstance() {
        return new SimilarFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_0, container, false);
    }

    public void update(){
        if (mAdapter!= null){
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(recyclerView);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        //permet un affichage sous forme liste verticale
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);


        //penser à passer notre Adapter (ici : Frg0Adapter) à un RecyclerViewMaterialAdapter
        mAdapter = new RecyclerViewMaterialAdapter(new SimilarAdapter(GPNApp.getTasks(), getActivity()));
        mRecyclerView.getWidth();
        mRecyclerView.setAdapter(mAdapter);

        //notifier le MaterialViewPager qu'on va utiliser une RecyclerView
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddTaskActivity.class);
                startActivity(intent);
            }
        });
    }
}
