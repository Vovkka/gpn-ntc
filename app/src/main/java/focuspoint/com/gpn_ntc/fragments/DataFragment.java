package focuspoint.com.gpn_ntc.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.DriverChooseActivity;
import focuspoint.com.gpn_ntc.R;
import focuspoint.com.gpn_ntc.SUPERINTERFACE;
import focuspoint.com.gpn_ntc.TaskActivity;
import focuspoint.com.gpn_ntc.model.Resource;
import focuspoint.com.gpn_ntc.model.ResourceDriver;
import focuspoint.com.gpn_ntc.model.Stage;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;
import focuspoint.com.gpn_ntc.view.ResizableLayout;
import focuspoint.com.gpn_ntc.view.ResourceDriverView;
import focuspoint.com.gpn_ntc.view.TaskDriverView;

/**
 * Created by v_banko on 1/26/2017.
 */

public class DataFragment extends Fragment {

    @BindView(R.id.description) TextView description;
    @BindView(R.id.task_drivers_view) LinearLayout taskDriversView;
    @BindView(R.id.resource_drivers_view) LinearLayout resourceDriversView;
    @BindView(R.id.hardnessCoefficient_view) TextView hadnessCoeffView;
    @BindView(R.id.constantCoefficient_view) TextView constantCoeffView;
    @BindView(R.id.constantValue_view) TextView constantValueView;
    @BindView(R.id.hardnessCoefficient_edit_view) TextView hadnessCoeffEditView;
    @BindView(R.id.constantCoefficient_edit_view) TextView constantCoeffEditView;
    @BindView(R.id.constantValue_edit_view) TextView constantValueEditView;
    @BindView(R.id.resource_view_spinner) Spinner resourceView;
    @BindView(R.id.fab_add_task_driver) FloatingActionButton fabTaskDriver;

    @BindView(R.id.task_driver_frame) LinearLayout taskDriverFrame;
    @BindView(R.id.resource_frame) LinearLayout resourceFrame;
//    @BindView(R.id.task_calculated_cost) TextView taskCalculatedCost;
//    @BindView(R.id.task_real_cost) TextView taskRealCost;
    @BindView(R.id.coeff_bar) View coeffBar;
    @BindView(R.id.resizable_layout) ResizableLayout rootView;

    @BindView(R.id.coefficient_tab_content) View coefftabContent;
    @BindView(R.id.driver_tab_content) View drivetabContent;
    @BindView(R.id.resource_tab_content) View resourcetabContent;




    @BindString(R.string.hardnessCoefficient) String hardCoeffString;
    @BindString(R.string.constantCoefficient) String constCoeffString;
    @BindString(R.string.no_more_drivers) String noMoreDrivers;
    @BindString(R.string.constantValue) String constValueString;
    @BindString(R.string.save_changes) String saveChangesString;
    @BindString(R.string.hh) String hh;

    private Task task;

    private boolean changes = false;
    private long start;
    private SUPERINTERFACE superinterface;

    public static final int DRIVER_TAB = 0;
    public static final int RESOURCE_TAB = 1;
    public static final int COEFFICIENT_TAB = 2;

    private int type;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.content_task, container, false);

        ButterKnife.bind(this, content);


        rootView.setKeyboardClose(this::takeFocusBack);
        rootView.setKeyboardOpen(() -> {});

        fabTaskDriver.setOnClickListener(view -> superinterface.addTaskFromActivity());

        hadnessCoeffEditView.setOnFocusChangeListener(textViewFocusListener);
        constantCoeffEditView.setOnFocusChangeListener(textViewFocusListener);
        constantValueEditView.setOnFocusChangeListener(textViewFocusListener);

//        hadnessCoeffEditView.setOnEditorActionListener(onEditorActionListener);
//        constantCoeffEditView.setOnEditorActionListener(onEditorActionListener);
//        constantValueEditView.setOnEditorActionListener(onEditorActionListener);

        resourceFrame.setOnClickListener(view -> {});

        fillData(true);
        resourceChooser();
        new Handler().postDelayed(() -> changes = false, 1000);
        start = System.currentTimeMillis();
        return content;
    }

    public void setSuperInterface(SUPERINTERFACE superInterface){
        this.superinterface = superInterface;
    }

    private void resourceChooser() {

        try {
            String[] data = task.getResourceNames();
            if (data.length == 0) return;


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data) {
                @NonNull
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    View view = super.getView(position, convertView, parent);
                    TextView text = (TextView) view.findViewById(android.R.id.text1);
                    text.setTextColor(Color.BLACK);
                    view.setVisibility(task.getCurrentResource() == null ? View.GONE : View.VISIBLE);
                    return view;
                }
            };

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            resourceView.setAdapter(adapter);

            for (int i = 0; i < task.getResources().size(); i++) {
                if (task.getResources().get(i) == task.getCurrentResource()) {
                    resourceView.setSelection(i);
                    break;
                }
            }

            resourceView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    List<Resource> list = task.getResources();
                    if (task.getCurrentResource() != list.get(i)) {
                        task.setCurrentResource(list.get(i));
                        fillData(true);
                    }
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }catch (Exception e){}
    }

    private Task.TaskListener taskListener = changeDriversCount -> {
        changes = true;
        fillData(changeDriversCount);
    };


    public void takeFocusBack() {
        new Handler().postDelayed(() -> {
            try{rootView.clearFocus();
            }catch (Exception e){}}, 200);
    }


    private void fillTaskDriversList() {
        taskDriversView.removeAllViews();

        new Handler().post(() -> {
            for (TaskDriver driver : task.getTaskDrivers()) {
                TaskDriverView view = new TaskDriverView(getContext());
                view.setTask(task, driver);
                taskDriversView.addView(view);
            }
            taskDriversView.requestLayout();
        });

    }

    private void fillResourceDriversList() {
        resourceDriversView.removeAllViews();
        if (task.getCurrentResource() == null) return;
//        new Handler().post(() -> {
            for (ResourceDriver driver : task.getCurrentResource().getResourceDrivers()) {
                ResourceDriverView view = new ResourceDriverView(getContext());

                view.setTask(task, driver);
                resourceDriversView.addView(view);
            }
            resourceDriversView.requestLayout();
//        });
    }

    public void setTask (Task task){
        this.task = task;
    }
    private TextView.OnEditorActionListener onEditorActionListener =  new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
            System.out.println("ACTION");


            textView.clearFocus();
            return false;
        }
    };


//                if (textView.getText().length() == 0) {
//                    textView.setText("0");
//                    if (textView == hadnessCoeffEditView) task.setHardnessCoefficient(0);
//                    if (textView == constantCoeffEditView) task.setConstantCoefficient(0);
//                    if (textView == constantValueEditView) task.setConstantValue(0);
//                } else {
//                    float value = Float.valueOf(textView.getText().toString());
//                    if (textView == hadnessCoeffEditView) task.setHardnessCoefficient(value);
//                    if (textView == constantCoeffEditView) task.setConstantCoefficient(value);
//                    if (textView == constantValueEditView) task.setConstantValue(value);
//                }
//                return true;
//            }
//            return false;

//        }
//    };

    public void fillData(boolean updateLists) {
        try {

            setTabType();


//        toolbar.setTitle(task.getDescription());
//        toolbar.setTitle("Этап: " + task.getStage().getDescription(this));
            resourceView.setClickable(task.getStage() != Stage.done);
            resourceView.setEnabled(task.getStage() != Stage.done);

            fabTaskDriver.setVisibility(task.getStage() != Stage.done ? View.VISIBLE : View.GONE);

            description.setText(task.getDescription());
            description.setVisibility(View.GONE);

            hadnessCoeffView.setText(hardCoeffString);
            constantCoeffView.setText(constCoeffString);
            constantValueView.setText(constValueString);


            if (updateLists) {
                fillTaskDriversList();
                fillResourceDriversList();
            }

            hadnessCoeffEditView.setText(String.valueOf(task.getHardnessCoefficient()));
            constantCoeffEditView.setText(String.valueOf(task.getConstantCoefficient()));
            constantValueEditView.setText(String.valueOf(task.getConstantValue()));

            if (task.getStage().equals(Stage.done)) {
                superinterface.defineFabVisibility(false);
            }
        }catch (Exception e){}
    }

    private View.OnFocusChangeListener textViewFocusListener = (view, b) -> {
        if (!(view instanceof EditText)) return;
        TextView textView = (TextView) view;
        if (!b) {
            if (textView.getText().length() == 0) {
                textView.setText("0");
                if (textView == hadnessCoeffEditView) task.setHardnessCoefficient(0);
                if (textView == constantCoeffEditView) task.setConstantCoefficient(0);
                if (textView == constantValueEditView) task.setConstantValue(0);
            } else {
                float value = Float.valueOf(textView.getText().toString());
                if (textView == hadnessCoeffEditView) task.setHardnessCoefficient(value);
                if (textView == constantCoeffEditView) task.setConstantCoefficient(value);
                if (textView == constantValueEditView) task.setConstantValue(value);
            }
        }
    };

    private void setTabType(){
        try {
            if (type == DRIVER_TAB) {
                drivetabContent.setVisibility(View.VISIBLE);
            }

            if (type == RESOURCE_TAB) {
                resourcetabContent.setVisibility(View.VISIBLE);
            }

            if (type == COEFFICIENT_TAB) {
                coefftabContent.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){}
    }

    public void setTabType(int type){
        this.type = type;
    }

}
