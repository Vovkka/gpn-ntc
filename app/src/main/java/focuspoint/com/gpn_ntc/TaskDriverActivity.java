package focuspoint.com.gpn_ntc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;

public class TaskDriverActivity extends AppCompatActivity {

    private Task task;

    @BindView(R.id.task_driver_list) RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_driver);
        ButterKnife.bind(this);




        Intent intent = getIntent();
        task = GPNApp.getTask(intent.getExtras().getLong("id"));
        if (task == null) finish();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Драйверы работы к задаче № " + task.getId());
        setSupportActionBar(toolbar);



        recyclerView.setAdapter(new TaskDriversAdapter(task.getTaskDrivers()));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }





    class TaskDriversAdapter extends RecyclerView.Adapter{

        private List<TaskDriver> list;

        public TaskDriversAdapter(List<TaskDriver> list){
            this.list = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.task_driver_view, null);


            return new RecyclerView.ViewHolder(view) {
                @Override
                public String toString() {
                    return super.toString();
                }
            };
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            View rootView = holder.itemView;
            TaskDriver driver = list.get(position);
            ((TextView)rootView.findViewById(R.id.description)).setText(driver.getDescription());
            ((TextView)rootView.findViewById(R.id.weight_view)).setText("Вес = " + driver.getWeight());
            ((TextView)rootView.findViewById(R.id.value)).setText("Значение = " + driver.getDriverValue());
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }



}
