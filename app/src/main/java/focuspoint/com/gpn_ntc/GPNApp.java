package focuspoint.com.gpn_ntc;

import android.app.Application;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import focuspoint.com.gpn_ntc.model.Resource;
import focuspoint.com.gpn_ntc.model.Task;
import focuspoint.com.gpn_ntc.model.TaskDriver;
import focuspoint.com.gpn_ntc.utils.JSONResourceReader;

/**
 * Created by root on 22.01.17.
 */

public class GPNApp extends Application {

    private static List<Task> tasks;
    private static List<Resource> resources;
    private static List<TaskDriver> drivers;

    public static Task currentTask;

    @Override
    public void onCreate() {
        super.onCreate();
        getDataFromJSON();
    }

    private void getDataFromJSON(){
        tasks = Task.getFromJSON(getResources());
        drivers = TaskDriver.getAllFromJSON(this);
        resources = Resource.getAllFromJSON(this);
    }

    public static List<Task> getTasks() {
        return tasks;
    }

    public static List<Resource> getResource() {
        return resources;
    }

    public static List<TaskDriver> getDrivers() {
        return drivers;
    }

    public static Task getTask(long id){
        for (Task task : tasks){
            if (task.getId() == id){
                return task;
            }
        }
        return null;
    }
}
